# skeleton-db

## schema

A visual representation of the database layer.

Tool used is [MySQL Workbench](https://www.mysql.com/products/workbench/)

Tool allows to create visual table, databases and various settings.

Tool also allows us forward-generation process which translates visual table models into SQL code used for code generation.