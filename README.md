# skeleton-db

## Description

This project is fully dockerized. It can be started just by executing a single command:

    npm run compose

The only requirement is to have `Docker`, and `Node` installed.

### How to install required software?

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    brew cask install docker

    brew install node

## Docker

Internal port `3306`

External port `9306`

## What commands we have?

The relevant commands and shortcuts are in [`package.json`](./package.json) file.

## How to start the whole project?

To start all services together required for the project:

Download all three projects in the same root folder:


    mkdir skeleton
    cd skeleton
    git clone git@gitlab.com:Zanoshky/skeleton-db.git
    git clone git@gitlab.com:Zanoshky/skeleton-api.git
    git clone git@gitlab.com:Zanoshky/skeleton-web.git


Execute the command:

    npm run compose

## Way of working

- Run with MySQL Workbench Forward Generation Script
- Search and find and delete all 


    `USE `u265770206_piggy` ;


- Search and find and delete all


    `u265770206_piggy`.


- Search and find and delete all

    VISIBLE


## Versioning


    git commit -a -m ''
    
    npm version patch && git push

    npm version minor && git push

    npm version major && git push


## Troubleshoot
