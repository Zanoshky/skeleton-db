# skeleton-db

## database

Organized SQL files that needs to be deployed in a single folder.

When pushing, or merging code to master branch, the GitLab will trigger the gitlab-ci flow that is defined in the file [.gitlab-ci.yml](../.gitlab-ci.yml).

Please order the files with integer prefix in numerical ascending order, like `1_`, `2_`, `3,_`...

The files will be executed in that order.