USE `u265770206_piggy` ;

-- -----------------------------------------------------
-- Data for table `user`
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO `users` (`id`, `active`, `registration_date`, `first_name`, `last_name`, `date_of_birth`, `manual_input`) VALUES
(1, 1, '2018-08-08 22:22:22', 'Marko', 'Zanoški', '1993-02-26', 1),
(2, 1, '2018-08-08 22:22:22', 'Marko', 'Zanoški', '1993-02-26', 1);

COMMIT;

START TRANSACTION;

INSERT INTO `web_identities` (`id`, `user_id`, `email`, `password`) VALUES
(DEFAULT, 1, 'asd@asd.com', '$2a$10$61137d7e138f21ce76fd3uy//tI4GT8PwWdn/k4XMIvKGEXCPln5G'),
(DEFAULT, 2, 'a@a.com', '$2a$10$a8e07ba36439e54b8345bugTzbg83iV/4BgX1gcWJ3YLXcgdeZpTO');

COMMIT;

START TRANSACTION;

INSERT INTO `metrics` (`user_id`, `type`, `value`, `date`) VALUES
(2, 'Income', 2598.48, '2021-05-24 00:00:00'),
(2, 'Income', 2562.48, '2021-04-27 00:00:00'),
(2, 'Income', 5324.96, '2021-03-26 00:00:00'),
(2, 'Income', 2458.89, '2021-02-25 00:00:00'),
(2, 'Income', 2462.89, '2021-01-28 00:00:00'),
(2, 'Utilities', 1894.00, '2021-01-01 00:00:00'),
(2, 'Shopping', 410.00, '2021-01-01 00:00:00'),
(2, 'Groceries', 140.00, '2021-01-01 00:00:00'),
(2, 'Health', 82.00, '2021-01-01 00:00:00'),
(2, 'Transport', 78.00, '2021-01-01 00:00:00'),
(2, 'Restaurants', 49.00, '2021-01-01 00:00:00'),
(2, 'Utilities', 1491.00, '2021-02-01 00:00:00'),
(2, 'Raminata', 1500.00, '2021-02-01 00:00:00'),
(2, 'Shopping', 878.00, '2021-02-01 00:00:00'),
(2, 'Groceries', 160.00, '2021-02-01 00:00:00'),
(2, 'Health', 90.00, '2021-02-01 00:00:00'),
(2, 'Utilities', 1599.00, '2021-03-01 00:00:00'),
(2, 'Shopping', 884.00, '2021-03-01 00:00:00'),
(2, 'Raminata', 500.00, '2021-03-01 00:00:00');

COMMIT;
